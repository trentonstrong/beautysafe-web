#
# Cookbook Name:: beautysafe-app
# Recipe:: default
#
# Copyright 2011, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

python_virtualenv node[:app][:deploy_path] do
  owner "vagrant"
  group "vagrant"
  action :create
end

# create a postgresql database with additional parameters
postgresql_database 'beautyapp' do
  connection {
      :host => "127.0.0.1",
      :port => 5432,
      :username => 'postgres',
      :password => node['postgresql']['password']['postgres']}
  template 'template0'
  encoding 'UTF-8'
  tablespace 'DEFAULT'
  connection_limit '-1'
  owner 'postgres'
  action :create
end
