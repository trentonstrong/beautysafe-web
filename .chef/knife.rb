log_level                :info
log_location             STDOUT
node_name                'trentstrong'
client_key               '/Users/trentstrong/Development/beautysafe/beautysafe-web/.chef/trentstrong.pem'
validation_client_name   'chef-validator'
validation_key           '/etc/chef/validation.pem'
chef_server_url          'http://Trent-Strongs-Mac-Pro.local:4000'
cache_type               'BasicFile'
cache_options( :path => '/Users/trentstrong/Development/beautysafe/beautysafe-web/.chef/checksums' )
