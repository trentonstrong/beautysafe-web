from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from scrapy import log
from urlparse import urljoin, urlsplit, parse_qs
from itertools import chain
from string import strip

SEPHORA_ROOT = 'http://www.sephora.com'


class SephoraSpider(BaseSpider):
    """
    Scraper for starting at brand list and ingesting product links
    """
    name = "sephora.com"

    start_urls = ['http://www.sephora.com/brands/complete_list.jhtml']

    def parse(self, response):
        doc = HtmlXPathSelector(response)
        brand_links = doc.select('//a[contains(@href, "brand_hierarchy.jhtml")]')
        log.msg('Got %d brands' % len(brand_links))
        for link in brand_links:
            url = urljoin(SEPHORA_ROOT, link.select('@href').extract()[0])
            brand = link.select('.//text()').extract()[0]
            brand = brand.lower().strip()
            meta = {'brand': brand}
            callback = self.parse_brand
            # Sephora Collection page is extremely tricked out
            if brand == "sephora collection":
                callback = self.parse_sephora_collection

            yield Request(url, callback=callback, meta=meta)

    def parse_brand(self, response):
        meta = response.meta
        brand = meta['brand']
        doc = HtmlXPathSelector(response)
        selector = '//a[contains(@href, "brand_hierarchy.jhtml?brandId=%s&categoryId")]' % brand
        category_links = doc.select(selector)
        for link in category_links:
            url = urljoin(SEPHORA_ROOT, link.select('@href').extract()[0])
            url += '&view=all'
            category = link.select('.//text()').extract()[0]
            category = category.lower().strip()
            # Sets are Sephora specific, groupings of items we will already ingest
            # The complete list is also a vanity category
            if "set" in category or "complete list" in category:
                continue
            meta['category'] = category
            yield Request(url, callback=self.parse_category, meta=meta)

    def parse_sephora_collection(self, response):
        pass

    def parse_category(self, response):
        meta = response.meta
        doc = HtmlXPathSelector(response)
        product_links = doc.select('//a[contains(@href, "product.jhtml")]')
        links_requested = set()
        for link in product_links:
            # don't process links twice
            if link in links_requested:
                continue
            links_requested.add(link)
            # Parse url and retrieve product id
            url = urljoin(SEPHORA_ROOT, link.select('@href').extract()[0])
            yield Request(url, callback=self.parse_product, meta=meta)

    SKIP_PRODUCTS = (
        'P37590', # Gift card
    )

    def parse_product(self, response):
        from scrapy.shell import inspect_response
        inspect_response(response)
        meta = response.meta
        doc = HtmlXPathSelector(response)

        # Product Id (SKU)
        url = response.url
        components = urlsplit(url)
        qs = parse_qs(components[3])
        product_id = qs['id'][0]
        if product_id in SephoraSpider.SKIP_PRODUCTS:
            return None
        # TODO: Remove this debugging code soon.  Goddamn Scrapy and their untestable framework...
        if not product_id:
            from scrapy.shell import inspect_response
            inspect_response(response)

        # Primary product name (color way / ingredient information may make this multi-valued
        # down below)
        copy = doc.select('//span[contains(@class, "copy12")]')
        product_name = copy.select('.//text()').extract()

        # Product description
        desc = copy.select('../text()').extract()
        headers = copy.select('../b').extract()
        # Functional stuff ahoy, reconstruct the sephora product description humpty dumpty
        not_whitespace = lambda s: s != ""
        desc = filter(not_whitespace, map(strip, desc))
        desc = "\n".join(chain.from_iterable(zip(headers, desc)))

        # Product image
        img_name = "%s_hero" % product_id
        doc.select('//img[contains(@src, "%s")]' % img_name)

        # Product ingredients... more zip-filter-mapping ahead...

