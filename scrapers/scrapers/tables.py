"""
Define products table (reflected) for scraping results
"""

from sqlalchemy import *
from database import metadata

urls = Table('scraper_urls', metadata,
    Column('id', Integer, primary_key=True),
    Column('url', Text, nullable=False),
    Column('spider', String(80), nullable=False),
    Column('fetched_at', DateTime, index=True, nullable=True),
    Column('frequency', Interval, nullable=False),
    UniqueConstraint('url', 'spider'))
