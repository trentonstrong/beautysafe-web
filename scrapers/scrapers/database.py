from sqlalchemy import create_engine
from sqlalchemy.schema import MetaData

connection_string = 'postgresql://postgres:dev@127.0.0.1:5432/beautyapp'
db = create_engine(connection_string)
metadata = MetaData()
metadata.bind = db
