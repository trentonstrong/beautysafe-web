import sys
import requests
from requests import async
import json

from shared import tofixture, fetch_annex, parse_list_table, parse_detail_table, ROOT_URL


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    # Fetch ingredient detail URLs
    start = 1
    urls = []
    print '# Starting URL crawl'
    while (True):
        resp = fetch_annex('III', start)
        if resp.status_code != 200:
            continue

        content = resp.content
        rows = parse_list_table(content, row_start=4)
        if rows is None:
            break
        elif len(rows) > 0:
            page_urls = [row[1][0].get('href') for row in rows]
            urls.extend(page_urls)
            print '# Crawled %d urls' % len(urls)
        if len(rows) < 100:
            break
        start += 100

    # Fetch detail pages asynchronously, parse them on response
    print '# Fetching URLs'

    def on_response(response):
        print '# ' + response.request.url

    detail_requests = [async.get(ROOT_URL + u, hooks=dict(response=on_response)) for u in urls]
    detail_responses = async.map(detail_requests, size=5)
    details = [parse_detail_table(response.content) for response in detail_responses]

    ingredients = []
    for (idx, detail) in enumerate(details):
        sys.stdout.flush()
        uses = detail[9][1].text_content().strip().lower()
        concentration = detail[10][1].text_content().strip().lower()
        miscellaneous = detail[11][1].text_content().strip().lower()
        warning_labels = detail[12][1].text_content().strip().lower()
        sccs_opinions = []
        # Denormalize opinions as json
        for link in detail[13][1].xpath('.//a'):
            sccs_opinions.append({
                'title': link.text_content().strip().lower(),
                'url': link.get('href')
            })
        sccs_opinions = json.dumps(sccs_opinions)

        # Fetch ingredient pages synchronously, parse and add ingredient information
        for link in detail[15][1].xpath('.//a'):
            url = link.get('href')
            response = requests.get(ROOT_URL + url)
            ingredient_details = parse_detail_table(response.content)
            ingredients.append({
                'name': ingredient_details[0][1].text_content().strip().lower(),
                'category': 0,
                # Denormalize functions as JSON as well
                'functions': json.dumps([link.text_content().strip().lower() for link in ingredient_details[9][1].xpath('.//a')]),
                'uses': uses,
                'concentration': concentration,
                'miscellaneous': miscellaneous,
                'warning_labels': warning_labels,
                'sccs_opinions': sccs_opinions})
            print '# Got %d ingredients so far...' % len(ingredients)

    print json.dumps(tofixture('ingredients.Ingredient', ingredients))

if __name__ == "__main__":
    sys.exit(main())
