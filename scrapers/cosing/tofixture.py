import sys
import json


def main(argv=sys.argv[1:]):

    file = open(argv[0])

    idx = 0
    models = []
    for line in file:
        if line[0] == "#":
            continue

        result = json.loads(line)
        for ingredient in result['ingredients']:
            models.append({
                'model': 'ingredients.Ingredient',
                'pk': idx,
                'fields': {
                    'name': ingredient,
                    'category': 1,
                    'cas': ''
                }
            })

            idx += 1

    print json.dumps(models)


if __name__ == "__main__":
    sys.exit(main())
