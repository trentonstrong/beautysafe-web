import sys
import requests
import json

from shared import tofixture, parse_list_table, ROOT_URL


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    response = requests.post(ROOT_URL + 'index.cfm', data={
        'fuseaction': 'ref_data.functions'
    })

    functions = [
        dict(name=function[0].text_content().strip().lower(),
             description=function[1].text_content().strip().lower())
        for function
        in parse_list_table(response.content)]

    print json.dumps(tofixture('ingredients.Function', functions))

if __name__ == "__main__":
    sys.exit(main())
