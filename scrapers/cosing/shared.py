"""
Shared utilities for CosIng scrapers
"""

import requests
from lxml.html import fromstring

ROOT_URL = 'http://ec.europa.eu/consumers/cosmetics/cosing/'


def tofixture(model, objects):
    """
    Convers a list of dictionary like objects into fixtures for a given model name
    """
    models = []
    for (idx, object) in enumerate(objects):
        models.append({
            'model': model,
            'pk': idx,
            'fields': object
        })
    return models


def chunks(l, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


def parse_detail_table(content):
    """
    Parses a CosIng detail table and returns its values as a list of KV tuples
    """

    page = fromstring(content)
    keys = [td.text_content().strip() for td in page.xpath(".//td[@class='details-header']")]
    vals = [td for td in page.xpath(".//td[@class='details']")]
    kvs = zip(keys, vals)

    return kvs


def parse_list_table(content, row_start=2, row_end=-1):
    """
    Returns a list of lists, each list representing a single row of table cells
    """
    page = fromstring(content)
    rows = page.xpath(".//tr")
    rows = [row.xpath(".//td") for row in rows]
    if len(rows) > 0:
        return rows[row_start:row_end]
    else:
        return None


def fetch_annex(annex, start=1):
    return requests.post(ROOT_URL + 'index.cfm', data={
        'fuseaction': 'search.results',
        'search': 'true',
        'start': start,
        'part_no': 0,
        'search_simple_name': '',
        'search_type': 'SUB',
        'search_annex': annex,
        'search_scope': 0,
        'search_status': 1,
        'search_advanced_name': '',
        'search_cas': '',
        'search_einecs_elincs': '',
        'search_inn': '',
        'search_ph_eur': '',
        'search_opinion': '',
        'search_ref_no': '',
        'search_iupac': '',
        'search_description': '',
        'search_restriction': '',
        'search_function': '',
        'search_proposed': 'N',
        'search_directive': '',
        'search_publication_date': '',
        'search_directiveOther': 0,
        'search_restrictionOther': '',
        'search_version_no': 2
    })
