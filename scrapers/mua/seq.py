#!/usr/bin/python
import sys

def main(args):
    first = 1
    increment = 1
    if len(args) == 1:
        raise Exception("At least one argument must be given")
    elif len(args) == 2:
        last = int(args[1])
    elif len(args) == 3:
        first = int(args[1])
        last = int(args[2])
    elif len(args) == 4:
        first = int(args[1])
        increment = int(args[2])
        last = int(args[3])

    for x in xrange(first, last+1, increment):
        print x

    return 0
    

if __name__ == "__main__":
    sys.exit(main(sys.argv))
