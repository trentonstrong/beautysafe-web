#!/usr/bin/python

import sys
import os
import eventlet
eventlet.monkey_patch()
import requests
from lxml.html import fromstring
import json
import time

session = None

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def parse(content):
    doc = fromstring(content)
    product_div = doc.xpath("//div[@class='product-content']")

    contents = product_div[0]
    if contents.text_content() == "Product not found":
        return None

    title_el = contents.xpath("//h2")
    title = title_el[0].text_content()
    attributes = title.split(' - ')
    if len(attributes) == 3:
        (category, brand, name) = attributes 
        location = ""
    elif len(attributes) == 4:
        (category, location, brand, name) = attributes
    elif len(attributes) == 5:
        (category, location, dummy, brand, name) = attributes

    image_el = contents.xpath("//div[@class='product-images']")[0]
    image = ""
    if image_el[0].get("src") != "/art/spacer.gif":
        image = image_el[0].get("src")

    return {
        'category': category,
        'location': location,
        'brand': brand,
        'name': name,
        'image': image }



def fetch(urls):
    (product_url, ingredients_url) = urls
    while(session is None):
        pass
    return (product_url, requests.get(product_url, cookies=session), requests.get(ingredients_url, cookies=session))


def main(args):
    root_url = "http://www.makeupalley.com"

# auth
    r_auth = requests.get(
        root_url + "/account/login.asp?sendtourl=%2F&UserName=oklahomabythesea&Password=ttt4283RT")
    global session
    session = r_auth.cookies

# scrape
    product_url_format = root_url + "/product/showreview.asp?ItemId=%d"
    ingr_url_format = root_url + "/product/ingredients_ajax.asp?id=%d"
    
    if len(args) == 1:
        first = int(args[0])
    else:
        first = 1

    urls = [(product_url_format % id, ingr_url_format % id) for id in xrange(first, 160000)]
    pool = eventlet.GreenPool(5)

    for (url, resp, ing_resp) in pool.imap(fetch, urls):
        if resp.status_code != 200:
            continue
        product = parse(resp.content)
        if product is None:
            continue

        if ing_resp.status_code == 200 and ing_resp.content != "Not available":
            product.update({ 'ingredients': ing_resp.content })

        product.update({ 
            'source': 'http://www.makeupalley.com',
            'url': url,
            'timestamp': time.time() })
        try:
            print json.dumps(product)
        except Exception:
            pass


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
