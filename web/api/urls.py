from django.conf.urls.defaults import *
from piston.resource import Resource
from web.api.handlers import IngredientSearchHandler

ingredient_search_handler = Resource(IngredientSearchHandler)

urlpatterns = patterns('',
   url(r'^ingredients/search/', ingredient_search_handler),
)

