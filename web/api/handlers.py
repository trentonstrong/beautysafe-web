""" REST API handler classes for API endpoints
"""
from piston.handler import BaseHandler
from piston.utils import rc
from haystack.query import SearchQuerySet, Raw

from web.ingredients.models import Ingredient


# Two different formats required for unicode and string object translations
# Unicode excluded characters must be a dictionary mapping since query parameters
# are unicode: http://docs.python.org/library/stdtypes.html#str.translate
EXCLUDED_CHARS = "*"
EXCLUDED_TABLE = dict((ord(c), None) for c in EXCLUDED_CHARS)


def exclude_characters(s):
    if isinstance(s, str):
        return s.translate(None, EXCLUDED_CHARS)
    elif isinstance(s, unicode):
        return s.translate(EXCLUDED_TABLE)
    else:
        raise TypeError("exclude_characters: argument must be string or unicode")


class IngredientSearchHandler(BaseHandler):
    """ Handler for the ingredient search endpoint """
    allowed_methods = ('GET',)

    def read(self, request):
        """ Returns ingredient search results based on the query 'q'.
        Query is treated as a Lucene query string
        """
        q = request.GET.get('q', "")
        q = exclude_characters(q)
        if q == "":
            return rc.BAD_REQUEST

        ingredients_quoted = [
            ''.join(['"', ingredient.strip(), '"'])
            for ingredient in q.split(',')]
        ingredient_query = ' OR '.join(ingredients_quoted)

        results = SearchQuerySet().filter(content=Raw(ingredient_query)).models(Ingredient)

        return [result.get_stored_fields() for result in results]
