"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from web.api.handlers import exclude_characters


class ExcludeCharactersTestCase(TestCase):
    def test_exclude_characters(self):
        test = "he**llo"
        test_unicode = unicode("he**llo")
        expected = "hello"
        self.assertEqual(exclude_characters(test), expected)
        self.assertEqual(exclude_characters(test_unicode), unicode(expected))


class IngredientSearchHandlerTestCase(TestCase):
    def test_search(self):
        resp = self.client.get('/api/ingredients/search/')
        self.assertEqual(resp.status_code, 400)

        resp = self.client.get('/api/ingredients/search/?q=*')
        self.assertEqual(resp.status_code, 400)
