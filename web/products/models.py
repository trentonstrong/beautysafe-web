"""
"""
from django.utils.translation import ugettext as _
from django.db import models


# Create your models here.
class Product(models.Model):
    """
    A product  from our beautiful database
    """
    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __unicode__(self):
        return self.name + '(' + self.sku + ')'

    name = models.CharField(max_length=255)

    description = models.TextField()

    image_url = models.URLField()

    sku = models.CharField(max_length=40)

    categories = models.ManyToManyField('Category')

    ingredients = models.ManyToManyField('ingredients.Ingredient')


class Category(models.Model):
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __unicode__(self):
        return self.name

    name = models.CharField(max_length=255, unique=True)

    description = models.TextField()
