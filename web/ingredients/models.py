"""
Models related to the ingredient search application
"""

from django.db import models
from django.utils.translation import ugettext as _


class Ingredient(models.Model):
    """
    A single regulated ingredient name
    """
    class Meta:
        verbose_name = _('Ingredient')
        verbose_name_plural = _('Ingredients')

    def __unicode__(self):
        return self.name

    # Category constants
    (RESTRICTED, BANNED, UNREGULATED) = (0, 1, 2)

    # ingredient common name, or complex chemical name
    name = models.TextField()

    # our category for the ingredient.  one of predefined category constants
    # defined above.
    category = models.IntegerField()

    # Chemical Abstracts Service identifier.  Optional, but can be useful to
    # find other information about the ingredient.
    cas = models.CharField(max_length=12, blank=True)

    # Array of functions, denormalized to JSON
    functions = models.TextField()

    # Text description of ingredient possible uses, body parts, etc...
    uses = models.TextField()

    # Maximum concentrations
    concentration = models.TextField()

    # Miscellaneous notes from CosIng entry
    miscellaneous = models.TextField()

    # Warnings which must be printed on product labels.  Usually reflects above.
    warning_labels = models.TextField()

    # Array of title/link pairs to scientific opinions on ingredient.
    # Denormalized as JSON
    sccs_opinions = models.TextField()


class Function(models.Model):
    """
    Defines functions that ingredients can serve
    """
    class Meta:
        verbose_name = _('Function')
        verbose_name_plural = _('Functions')

    def __unicode__(self):
        return self.name

    # Name of function, e.g. abrasive, detangling...
    name = models.CharField(max_length=255, unique=True)

    # Long text description of the function
    description = models.TextField()
