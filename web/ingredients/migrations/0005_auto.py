# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing index on 'Function', fields ['name']
        db.delete_index('ingredients_function', ['name'])


    def backwards(self, orm):
        
        # Adding index on 'Function', fields ['name']
        db.create_index('ingredients_function', ['name'])


    models = {
        'ingredients.function': {
            'Meta': {'object_name': 'Function'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'ingredients.ingredient': {
            'Meta': {'object_name': 'Ingredient'},
            'cas': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'category': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['ingredients']
