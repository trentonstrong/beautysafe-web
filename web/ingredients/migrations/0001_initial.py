# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Ingredient'
        db.create_table('ingredients_ingredient', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('category', self.gf('django.db.models.fields.IntegerField')()),
            ('cas', self.gf('django.db.models.fields.CharField')(max_length=12, blank=True)),
        ))
        db.send_create_signal('ingredients', ['Ingredient'])


    def backwards(self, orm):
        
        # Deleting model 'Ingredient'
        db.delete_table('ingredients_ingredient')


    models = {
        'ingredients.ingredient': {
            'Meta': {'object_name': 'Ingredient'},
            'cas': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'category': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['ingredients']
