# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Ingredient.functions'
        db.add_column('ingredients_ingredient', 'functions', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)

        # Adding field 'Ingredient.uses'
        db.add_column('ingredients_ingredient', 'uses', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)

        # Adding field 'Ingredient.concentration'
        db.add_column('ingredients_ingredient', 'concentration', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)

        # Adding field 'Ingredient.miscellaneous'
        db.add_column('ingredients_ingredient', 'miscellaneous', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)

        # Adding field 'Ingredient.warning_labels'
        db.add_column('ingredients_ingredient', 'warning_labels', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)

        # Adding field 'Ingredient.sccs_opinions'
        db.add_column('ingredients_ingredient', 'sccs_opinions', self.gf('django.db.models.fields.TextField')(default=''), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Ingredient.functions'
        db.delete_column('ingredients_ingredient', 'functions')

        # Deleting field 'Ingredient.uses'
        db.delete_column('ingredients_ingredient', 'uses')

        # Deleting field 'Ingredient.concentration'
        db.delete_column('ingredients_ingredient', 'concentration')

        # Deleting field 'Ingredient.miscellaneous'
        db.delete_column('ingredients_ingredient', 'miscellaneous')

        # Deleting field 'Ingredient.warning_labels'
        db.delete_column('ingredients_ingredient', 'warning_labels')

        # Deleting field 'Ingredient.sccs_opinions'
        db.delete_column('ingredients_ingredient', 'sccs_opinions')


    models = {
        'ingredients.function': {
            'Meta': {'object_name': 'Function'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'ingredients.ingredient': {
            'Meta': {'object_name': 'Ingredient'},
            'cas': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'category': ('django.db.models.fields.IntegerField', [], {}),
            'concentration': ('django.db.models.fields.TextField', [], {}),
            'functions': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'miscellaneous': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.TextField', [], {}),
            'sccs_opinions': ('django.db.models.fields.TextField', [], {}),
            'uses': ('django.db.models.fields.TextField', [], {}),
            'warning_labels': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['ingredients']
