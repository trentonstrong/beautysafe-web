# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Ingredient.name'
        db.alter_column('ingredients_ingredient', 'name', self.gf('django.db.models.fields.TextField')(max_length=255))


    def backwards(self, orm):
        
        # Changing field 'Ingredient.name'
        db.alter_column('ingredients_ingredient', 'name', self.gf('django.db.models.fields.CharField')(max_length=255))


    models = {
        'ingredients.ingredient': {
            'Meta': {'object_name': 'Ingredient'},
            'cas': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'category': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['ingredients']
