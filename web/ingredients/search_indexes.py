from haystack import indexes
from ingredients.models import Ingredient

try:
    import simplejson as json
except ImportError:
    import json


class IngredientIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Elastic search index for ingredients
    """

    def get_model(self):
        return Ingredient

    def prepare_functions(self, obj):
        return json.loads(obj.functions)

    text = indexes.CharField(document=True, model_attr='name')

    category = indexes.IntegerField(model_attr='category')

    cas = indexes.CharField(model_attr='cas')

    functions = indexes.MultiValueField()

    uses = indexes.CharField(model_attr='uses')

    concentration = indexes.CharField(model_attr='concentration')

    miscellaneous = indexes.CharField(model_attr='miscellaneous')

    warning_labels = indexes.CharField(model_attr='warning_labels')

    sccs_opinions = indexes.CharField(model_attr='sccs_opinions')
