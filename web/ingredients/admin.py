from ingredients.models import Ingredient
from django.contrib import admin

admin.site.register(Ingredient)


class IngredientAdmin():
    list_display = ('name',)
