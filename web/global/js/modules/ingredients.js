/* Author:

*/
var bs = bs = window.bs || {};

bs.Ingredient = Backbone.Model.extend({});


bs.IngredientSearchResultsCollection = Backbone.Collection.extend({
  url: '/api/ingredients/search/',
  model: bs.Ingredient
});


bs.IngredientSearchResultsView = Backbone.View.extend({
    tagName: 'div',
    className: 'search-results span4',
    events: {
        'reset': 'render'
    },
    initialize: function() {
        _.bindAll(this);
        if (!this.options.collection) {
            throw 'Must define collection!';
        }
        this.collection.bind('reset', this.render);
        this.template = _.template($('#ingredient_results').html());
    },

    render: function() {
        $(this.el).html(this.template({
            results: this.collection.toJSON()
        }));
        $(this.el).find('li').click(function (event) {
           $(this).find('dl').toggle();
        });
        return this;
    }
});


bs.IngredientSearchView = Backbone.View.extend({
    events: {
        'click button.search': 'doSearch',
        'click button.reset': 'clearResults'
    },

    initialize: function() {
        _.bindAll(this);
        this.results = new bs.IngredientSearchResultsCollection();
        this.resultsView = new bs.IngredientSearchResultsView({
            collection: this.results
        });

        this.searchResultsEl = $(this.resultsView.render().el).hide();
        this.el.append(this.searchResultsEl);
        this.textinput = this.$('textarea.input');
    },

    doSearch: function() {
        var query = $.trim(this.textinput.val());
        if (query === "") {
            return;
        }

        var searchCallback = _.bind(function() {
            this.searchResultsEl.show();
            this.$('button.reset').show();
        }, this);

        this.results.fetch({
            data: {
                q: query
            },
            success: searchCallback });
    },

    clearResults: function() {
        this.searchResultsEl.hide();
        this.textinput.val("");
    }
});

$(document).ready(function() {
    var searchView = new bs.IngredientSearchView({
        el: $('#ingredient_search')
    });
});
